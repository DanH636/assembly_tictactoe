.386
.model flat, stdcall

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;includem biblioteci, si declaram ce functii vrem sa importam

includelib msvcrt.lib

extern exit: proc

extern malloc: proc

extern memset: proc


includelib canvas.lib

extern BeginDrawing: proc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;declaram simbolul start ca public - de acolo incepe executia

public start

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;sectiunile programului, date, respectiv cod

.data

;aici declaram date
window_title DB "Tic-Tac-Toe",0

area_width EQU 640
tic_table db 0,0,0,
					 0,0,0,
					 0,0,0
					 
wins1 db 0
wins2 db 0
tab_sf db 0
area_height EQU 480

area DD 0



arg1 EQU 8
arg2 EQU 12
arg3 EQU 16
arg4 EQU 20
arg5 EQU 24
arg6 EQU 28
count_click db 0
start_game db 0

symbol_width EQU 10
symbol_height EQU 20
include digits.inc
include letters.inc

.code
verif_tab_sfar proc
push ebp
mov ebp,esp
pusha
mov ecx,9
mov eax,0
t1:
mov ebx,ecx
dec ebx
add al,[tic_table+ebx]
loop t1
mov tab_sf,al
popa
mov esp,ebp
pop ebp
ret
verif_tab_sfar endp
;curatam tabela
;curatam fiecare patrat 
clean_table proc
push ebp
mov ebp,esp
pusha
mov ecx,9
et:
mov ebx,ecx
dec ebx
mov [tic_table+ebx],0
loop et
popa
mov esp,ebp
pop ebp
ret
clean_table endp

verif_tic proc
push ebp
mov ebp,esp
pusha
mov ebx,0
mov [ebp-4],ebx
mov ecx,3
et2:
mov [ebp-8], ecx
mov ecx,3
mov eax,0
et1:
mov ebx,[ebp-4]
add ebx,ecx
dec ebx
mov dl,[tic_table+ebx]
cmp dl,0
je nur
add al,[tic_table+ebx]
loop et1
mov cl,3
div cl
cmp ah,0
jne nur
cmp al,1
je da
cmp al,2
je da2
nur:
mov ebx,[ebp-4]
add ebx,3
mov [ebp-4],ebx
mov ecx,[ebp-8]
loop et2

mov ebx,0
mov [ebp-4],ebx
mov ecx,3
eti1:
mov [ebp-8],ecx
mov ecx,3
mov eax,0
mov ebx,[ebp-4]
eti2:
mov dl,[tic_table+ebx]
cmp dl,0
je nur2
add al,[tic_table+ebx]
add ebx,3
loop eti2
mov cl,3
div cl
cmp ah,0
jne nur2
cmp al,1
je da
cmp al,2
je da2
nur2:
mov ebx,[ebp-4]
add ebx,1
mov [ebp-4],ebx
mov ecx,[ebp-8]
loop eti1

;verificarea diagonaeli principale
mov eax,0
mov dl,[tic_table+0]
cmp dl,0
je nu1
add al,[tic_table+0]
mov dl,[tic_table+4]
cmp dl,0
je nu1
add al,[tic_table+4]
mov dl,[tic_table+8]
cmp dl,0
je nu1
add al,[tic_table+8]
mov cl,3
div cl
cmp ah,0
jne nu1
cmp al,1
je da
cmp al,2
je da2

nu1:
;verificarea diagonaeli secundare
mov eax,0
mov dl,tic_table[2]
cmp dl,0
je nu
add al,tic_table[2]
mov dl,tic_table[4]
cmp dl,0
je nu
add al,tic_table[4]
mov dl,tic_table[6]
cmp dl,0
je nu
add al,tic_table[6]
mov cl,3
div cl
cmp ah,0
jne nu
cmp al,1
je da
cmp al,2
je da2
jmp nu
da:
mov dl,1
mov wins1,dl
jmp nu

da2:
mov dl,2
mov wins2,dl
nu:
popa
mov esp,ebp
pop ebp
ret
verif_tic endp

;arg1 x
;arg2 y
;arg3 lung
draw_o proc
push ebp
mov ebp,esp
pusha
mov edi,area
mov eax,[ebp+arg2]
mov ebx,[ebp+arg1]
mov ecx,area_width
shl ecx,2
mul ecx
shl ebx,2
add eax,ebx
add edi,eax


mov ecx,[ebp+arg3]
mov eax,area_width
shl eax,2
mul ecx
push eax
mov edx,0
mov ebx,0

etic3:
pop eax
push eax
add eax,edx
mov [edi+eax],ebx
pop eax
push eax
sub eax,edx
mov [edi+eax],ebx
pop eax
push eax
not eax
add eax,1
add eax,edx
mov [edi+eax],ebx
pop eax
push eax
not eax
add eax,1
sub eax,edx
mov [edi+eax],ebx
add edx,4
loop etic3
pop eax
mov eax,area_width
mov ecx,[ebp+arg3]
shl eax,2
push eax
eticheta:
pop eax

push eax
mul ecx
push eax
push eax
mov edx,[ebp+arg3]
shl edx,2
add eax,edx
mov [edi+eax],ebx
pop eax
push eax
not eax
inc eax
add eax,edx
mov [edi+eax],ebx
pop eax
not eax
inc eax
sub eax,edx
mov [edi+eax],ebx
pop eax
sub eax,edx
mov [edi+eax],ebx
loop eticheta
mov [edi+edx],edx
sub edi,edx
mov [edi],edx
pop eax
popa
mov esp,ebp
pop ebp
ret 12
draw_o endp

;arg1 x
;arg2 y
;arg3 lung
draw_x proc
push ebp
mov ebp,esp
pusha
mov edi,area
mov eax,[ebp+arg2]
mov ebx,[ebp+arg1]
mov ecx,area_width
shl ecx,2
mul ecx
shl ebx,2
add eax,ebx
add edi,eax

mov ecx,4
etic2:
push ecx
mov ecx,[ebp+arg3]
etic1:
mov eax,area_width
shl eax,2
mul ecx
push eax
mov ebx,ecx
shl ebx,2
add eax,ebx
mov edx,0h
mov [edi+eax],edx
pop eax
push eax
sub eax,ebx
mov [edi+eax],edx
pop eax
push eax
add eax, ebx
not eax
add eax,1
mov [edi+eax],edx
pop eax
not eax
add eax,1
add eax,ebx
mov [edi+eax],edx
loop etic1
mov [edi],edx
add edi,4
pop ecx
loop etic2
popa
mov esp,ebp
pop ebp
ret 12
draw_x endp
;arg1 x
;arg2 y
;arg3 lungime
;arg4 inaltime
;
;procedura care inlocuieste o arie de arg3*arg4 cu alb incepan cu coordonatele (arg1,arg2)

clean_area proc
push ebp
mov ebp,esp
push 0
pusha
mov edi,area
mov [ebp-4],edi
mov eax,[ebp+arg2]
mov ebx,[ebp+arg1]
mov ecx,area_width
shl ecx,2
mul ecx
add edi,eax
shl ebx,2
add edi,ebx

mov ecx,[ebp+arg4]
linii:
push ecx
mov ecx,[ebp+arg3]
coloane:
mov eax,4
mul ecx
mov ebx,0FFFFFFh
mov [edi+eax],ebx
loop coloane
mov ecx,area_width
shl ecx,2
add edi,ecx
pop ecx
loop linii
popa
mov esp,ebp
pop ebp
ret 16
clean_area endp

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;arg1 x
;arg2 y
;arg3 lungime 
;
;procedura care deseneaza tabela de 3x3
;latura unui patrat din tabela este de 100 pixeli
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
draw_table proc
push ebp
mov ebp ,esp
push 0
pusha
mov eax,[ebp+arg2]
mov ebx,[ebp+arg1]
mov ecx,area_width
shl ecx,2
mov edx,0
mul ecx
shl ebx,2
add edi,eax
add edi,ebx
mov [ebp-4],edi
mov ecx,4
;desenam de patru ori linia orizontala
eti1:
push ecx
mov ecx,2
;facem dubla linia orizontala 
et3:
push ecx
mov ecx,[ebp+arg3]
sub ecx,96
shl ecx,2
;bucla pentru desenarea unei lini orizontale
et1:
mov eax,0
mov [edi+ecx],eax
loop et1
mov ecx,area_width
shl ecx,2
add edi,ecx
pop ecx
loop et3
mov eax,area_width
mov ebx,[ebp+arg3]
mul ebx
add edi,eax
pop ecx
loop eti1

mov edi,[ebp-4]
mov ecx,area_width
shl ecx,2
sub edi,ecx
sub edi,8

mov ecx,4
;punem de patru ori linia orizontala pe ecran
etic3:
push ecx
mov ecx,2
;bucla pentru dublarea liniei orrizontale
etic2:
push ecx
mov ecx,[ebp+arg3]
sub ecx,92
;bucla pentru desenarea liniei orizontale
etic1:
mov eax,area_width
shl eax,2
mul ecx
mov ebx,0
mov [edi+eax],ebx
loop etic1
add edi,4
pop ecx
loop etic2
mov ecx,[ebp+arg3]
add edi,ecx
pop ecx
loop etic3
popa
mov esp,ebp
pop ebp
ret 12
draw_table endp

;arg1 x
;arg2 y
;
control_table_o proc
push ebp
mov ebp,esp
pusha
mov eax,[ebp+arg1]
sub eax,180
mov cx,100
mov edx,0
div cx
mov [ebp-8],eax
inc eax
mov cx,100
mul cx
sub eax,50
add eax,180
mov [ebp-4],eax
mov eax,[ebp+arg2]
sub eax,50
mov edx,0
mov cx,100
div cx
mov [ebp-12],eax
inc eax
mov cx,100
mul cx
mov ebx,[ebp-4]
push 40
push eax
push ebx
call draw_o
push 39
push eax
push ebx
call draw_o
mov eax,[ebp-12]
mov cl,3
mul cl
mov ebx,[ebp-8]
add eax,ebx
mov bl,2
mov [tic_table+eax],bl
popa
mov esp,ebp
pop ebp
ret 8
control_table_o endp

control_table_x proc
push ebp
mov ebp,esp
pusha
mov eax,[ebp+arg1]
sub eax,180
mov cx,100
mov edx,0
div cx
mov [ebp-8],eax
inc eax
mov cx,100
mul cx
sub eax,50
add eax,180
mov [ebp-4],eax
mov eax,[ebp+arg2]
sub eax,50
mov edx,0
mov cx,100
div cx
mov [ebp-12],eax
inc eax
mov cx,100
mul cx
mov ebx,[ebp-4]
push 40
push eax
push ebx
call draw_x
mov eax,[ebp-12]
mov cl,3
mul cl
mov ebx,[ebp-8]
add eax,ebx
mov bl,1
mov [tic_table+eax],bl
popa
mov esp,ebp
pop ebp
ret 8
control_table_x endp
; procedura make_text afiseaza o litera sau o cifra la coordonatele date
; arg1 - simbolul de afisat (litera sau cifra)
; arg2 - pointer la vectorul de pixeli
; arg3 - pos_x
; arg4 - pos_y
;arg5 - col_t
;arg6 - col_f
make_text proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1] ; citim simbolul de afisat
	cmp eax, 'A'
	jl make_digit
	cmp eax, 'Z'
	jg make_digit
	sub eax, 'A'
	lea esi, letters
	jmp draw_text
make_digit:
	cmp eax, '0'
	jl make_space
	cmp eax, '9'
	jg make_space
	sub eax, '0'
	lea esi, digits
	jmp draw_text
make_space:	
	mov eax, 26 ; de la 0 pana la 25 sunt litere, 26 e space
	lea esi, letters
	
draw_text:
	mov ebx, symbol_width
	mul ebx
	mov ebx, symbol_height
	mul ebx
	add esi, eax
	mov ecx, symbol_height
bucla_simbol_linii:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, symbol_height
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, symbol_width
bucla_simbol_coloane:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb
	mov eax,[ebp+arg5]
	mov dword ptr [edi], eax
	jmp simbol_pixel_next
simbol_pixel_alb:
	mov eax,[ebp+arg6]
	mov dword ptr [edi], eax
simbol_pixel_next:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane
	pop ecx
	loop bucla_simbol_linii
	popa
	mov esp, ebp
	pop ebp
	ret
make_text endp

; un macro ca sa apelam mai usor desenarea simbolului
make_text_macro macro symbol, drawArea, x, y,col_f,col_t
	push col_f
	push col_T
	push y
	push x
	push drawArea
	push symbol
	call make_text
	add esp, 24
endm
make_start macro 
make_text_macro 'S', area,  260,110,0FFFFFFh,0
	make_text_macro 'T', area,  270,110,0FFFFFFh,0
	make_text_macro 'A', area,  280,110,0FFFFFFh,0
	make_text_macro 'R', area,  290,110,0FFFFFFh,0
	make_text_macro 'T', area,  300,110,0FFFFFFh,0
	make_text_macro 26, area,  310,110	,0FFFFFFh,0
	make_text_macro 'G', area, 320,110,0FFFFFFh,0
	make_text_macro 'A', area, 330,110,0FFFFFFh,0
	make_text_macro 'M', area, 340,110,0FFFFFFh,0
	make_text_macro 'E', area, 350,110,0FFFFFFh,0
endm
; functia de desenare - se apeleaza la fiecare click
; sau la fiecare interval de 200ms in care nu s-a dat click
; arg1 - evt (0 - initializare, 1 - click, 2 - s-a scurs intervalul fara click)
; arg2 - x
; arg3 - y
draw proc
	
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1]
	cmp eax, 1
	je evt_click
	cmp eax, 2
	je verf_sf_joc; nu s-a efectuat click pe nimic
	;mai jos e codul care intializeaza fereastra cu pixeli albi
	
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	shl eax, 2
	push eax
	push 0FFh
	push area
	call memset
	add esp, 12
	jmp afisare_litere
	
evt_click: 

	mov edi, area
	mov ecx, area_height
	mov ebx, [ebp+arg2]
	
	mov al,start_game
	cmp al,1;verificam daca a fost desenata tabela sau daca al doilea jucator a introdus 0
	;daca a fost, primul player va introduce x 
	je player1
	mov al,start_game
	cmp al,2;verificam daca primul jucator a introdus x
	;daca a introdus, al doilea player va introduce 0
	;si se va reveni la primul jucator
	je player2
	
	;verificarea pentru apasarea butonului [start game]
	cmp ebx,260
	jl final_draw
	cmp ebx,350
	jg final_draw
	mov eax, [ebp+arg3]
	
	cmp eax,110
	jl final_draw
	cmp eax,130
	jg final_draw


	mov al,1
	mov start_game,al
	make_text_macro 'P', area,  50,110,000FF00h,0
	make_text_macro 'L', area,  60,110,000FF00h,0
	make_text_macro 'A', area,  70,110,000FF00h,0
	make_text_macro 'Y', area,  80,110,000FF00h,0
	make_text_macro 'E', area,  90,110,000FF00h,0
	make_text_macro 'R', area,  100,110,000FF00h,0
	make_text_macro ' ', area,  110,110,000FF00h,0
	make_text_macro '1', area,  120,110,000FF00h,0

	make_text_macro 'P', area,  50,210,0FFFFFFh,0
	make_text_macro 'L', area,  60,210,0FFFFFFh,0
	make_text_macro 'A', area,  70,210,0FFFFFFh,0
	make_text_macro 'Y', area,  80,210,0FFFFFFh,0
	make_text_macro 'E', area,  90,210,0FFFFFFh,0
	make_text_macro 'R', area,  100,210,0FFFFFFh,0
	make_text_macro ' ', area,  110,210,0FFFFFFh,0
	make_text_macro '2', area,  120,210,0FFFFFFh,0
	
	push dword ptr 50
	push dword ptr 140
	push dword ptr 110
	push dword ptr 240
	call clean_area
	
	push dword ptr 400
	push dword ptr 50
	push dword ptr 180
	call draw_table
	
	jmp final_draw

finish_game:
	
	push dword ptr 480
	push dword ptr 640
	push dword ptr 0
	push dword ptr 0
	call clean_area
	mov al,0
	;mov count_click,al
	mov start_game,al
	call clean_table
afisare_litere:
	
	;scriem start game
	make_start
	jmp final_draw
player1:
	mov eax,[ebp+arg3]
	cmp eax,50
	jl final_draw
	cmp eax,350
	jg final_draw
	mov eax,[ebp+arg2]
	cmp eax,180
	jl final_draw
	cmp eax,480
	jg final_draw

	push [ebp+arg3]
	push [ebp+arg2]
	call control_table_x
	mov al,2
	mov start_game,al
	make_text_macro 'P', area,  50,110,0FFFFFFh,0
	make_text_macro 'L', area,  60,110,0FFFFFFh,0
	make_text_macro 'A', area,  70,110,0FFFFFFh,0
	make_text_macro 'Y', area,  80,110,0FFFFFFh,0
	make_text_macro 'E', area,  90,110,0FFFFFFh,0
	make_text_macro 'R', area,  100,110,0FFFFFFh,0
	make_text_macro ' ', area,  110,110,0FFFFFFh,0
	make_text_macro '1', area,  120,110,0FFFFFFh,0

	make_text_macro 'P', area,  50,210,000FF00h,0
	make_text_macro 'L', area,  60,210,000FF00h,0
	make_text_macro 'A', area,  70,210,000FF00h,0
	make_text_macro 'Y', area,  80,210,000FF00h,0
	make_text_macro 'E', area,  90,210,000FF00h,0
	make_text_macro 'R', area,  100,210,000FF00h,0
	make_text_macro ' ', area,  110,210,000FF00h,0
	make_text_macro '2', area,  120,210,000FF00h,0
	jmp final_draw
player2:
	mov eax,[ebp+arg3]
	cmp eax,50
	jl final_draw
	cmp eax,350
	jg final_draw
	mov eax,[ebp+arg2]
	cmp eax,180
	jl final_draw
	cmp eax,480
	jg final_draw
	
	inc count_click
	
	push [ebp+arg3]
	push [ebp+arg2]
	call control_table_o

	make_text_macro 'P', area,  50,110,000FF00h,0
	make_text_macro 'L', area,  60,110,000FF00h,0
	make_text_macro 'A', area,  70,110,000FF00h,0
	make_text_macro 'Y', area,  80,110,000FF00h,0
	make_text_macro 'E', area,  90,110,000FF00h,0
	make_text_macro 'R', area,  100,110,000FF00h,0
	make_text_macro ' ', area,  110,110,000FF00h,0
	make_text_macro '1', area,  120,110,000FF00h,0

	make_text_macro 'P', area,  50,210,0FFFFFFh,0
	make_text_macro 'L', area,  60,210,0FFFFFFh,0
	make_text_macro 'A', area,  70,210,0FFFFFFh,0
	make_text_macro 'Y', area,  80,210,0FFFFFFh,0
	make_text_macro 'E', area,  90,210,0FFFFFFh,0
	make_text_macro 'R', area,  100,210,0FFFFFFh,0
	make_text_macro ' ', area,  110,210,0FFFFFFh,0
	make_text_macro '2', area,  120,210,0FFFFFFh,0
mov al,1
	mov start_game,al
	jmp final_draw

	verf_sf_joc:
	call verif_tic
	mov dl,wins1
	cmp dl,1
	je player_wins1
	mov dl,wins2
	cmp dl,2
	je player_wins2
	call verif_tab_sfar
	mov dl,tab_sf
	cmp dl,13
	je finish_game
	jg finish_game
	mov dl,0
	mov tab_sf,dl
	jmp final_draw
	player_wins1:
	push dword ptr 480
	push dword ptr 640
	push dword ptr 0
	push dword ptr 0
	call clean_area
	mov al,0
	mov count_click,al
	mov start_game,al
	call clean_table
	make_start
	make_text_macro 'P', area,  250,140,000FF00h,0
	make_text_macro 'L', area,  260,140,000FF00h,0
	make_text_macro 'A', area,  270,140,000FF00h,0
	make_text_macro 'Y', area,  280,140,000FF00h,0
	make_text_macro 'E', area,  290,140,000FF00h,0
	make_text_macro 'R', area,  300,140,000FF00h,0
	make_text_macro ' ', area,  310,140,000FF00h,0
	make_text_macro '1', area,  320,140,000FF00h,0
	make_text_macro ' ', area,  330,140,000FF00h,0
	make_text_macro 'W', area,  340,140,000FF00h,0
	make_text_macro 'I', area,  350,140,000FF00h,0
	make_text_macro 'N', area,  360,140,000FF00h,0
	make_text_macro 'S', area,  370,140,000FF00h,0
	mov dl,0
	mov wins1,dl
	mov wins2,dl
	call clean_table
	jmp final_draw
	player_wins2:
	push dword ptr 480
	push dword ptr 640
	push dword ptr 0
	push dword ptr 0
	call clean_area
	mov al,0
	mov count_click,al
	mov start_game,al
	call clean_table
make_start
	make_text_macro 'P', area,  250,140,000FF00h,0
	make_text_macro 'L', area,  260,140,000FF00h,0
	make_text_macro 'A', area,  270,140,000FF00h,0
	make_text_macro 'Y', area,  280,140,000FF00h,0
	make_text_macro 'E', area,  290,140,000FF00h,0
	make_text_macro 'R', area,  300,140,000FF00h,0
	make_text_macro ' ', area,  310,140,000FF00h,0
	make_text_macro '2', area,  320,140,000FF00h,0
	make_text_macro ' ', area,  330,140,000FF00h,0
	make_text_macro 'W', area,  340,140,000FF00h,0
	make_text_macro 'I', area,  350,140,000FF00h,0
	make_text_macro 'N', area,  360,140,000FF00h,0
	make_text_macro 'S', area,  370,140,000FF00h,0
	mov dl,0
	mov wins1,dl
	mov wins2,dl
	call clean_table
	jmp final_draw
	final_draw:
	popa
	mov esp, ebp
	pop ebp
	ret
draw endp

start:
	;alocam memorie pentru zona de desenat
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	shl eax, 2
	push eax
	call malloc
	add esp, 4
	mov area, eax
	;apelam functia de desenare a ferestrei
	; typedef void (*DrawFunc)(int evt, int x, int y);
	; void __cdecl BeginDrawing(const char *title, int width, int height, unsigned int *area, DrawFunc draw);
	push offset draw
	push area
	push area_height
	push area_width
	push offset window_title
	call BeginDrawing
	add esp, 20
	
	;terminarea programului
	push 0
	call exit
end start
